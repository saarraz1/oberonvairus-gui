import Dialog from "../node_modules/@material-ui/core/Dialog/Dialog";
import React from "react";
import {api} from "./api";
import Grid from "../node_modules/@material-ui/core/Grid/Grid";
import Autocomplete from "../node_modules/@material-ui/lab/Autocomplete/Autocomplete";
import TextField from "../node_modules/@material-ui/core/TextField/TextField";
import Button from "../node_modules/@material-ui/core/Button/Button";
import DialogTitle from "../node_modules/@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "../node_modules/@material-ui/core/DialogContent/DialogContent";
import List from "../node_modules/@material-ui/core/List/List";
import ListItem from "../node_modules/@material-ui/core/ListItem/ListItem";
import ListItemIcon from "../node_modules/@material-ui/core/ListItemIcon/ListItemIcon";
import {Accessible, Add, AirlineSeatFlat, Clear, School} from "@material-ui/icons";
import ListItemText from "../node_modules/@material-ui/core/ListItemText/ListItemText";
import Typography from "../node_modules/@material-ui/core/Typography/Typography";
import Slider from "../node_modules/@material-ui/core/Slider/Slider";
import ListItemSecondaryAction from "../node_modules/@material-ui/core/ListItemSecondaryAction/ListItemSecondaryAction";
import IconButton from "../node_modules/@material-ui/core/IconButton/IconButton";
import Select from "../node_modules/@material-ui/core/Select/Select";
import MenuItem from "../node_modules/@material-ui/core/MenuItem/MenuItem";
import Tooltip from "../node_modules/@material-ui/core/Tooltip/Tooltip";
import InputAdornment from "../node_modules/@material-ui/core/InputAdornment/InputAdornment";


const INTERVENTIONS = {
    elderly_distancing: {name: 'Elderly distancing', icon: <Accessible />, is_timed: true},
    school_closing: {name: 'School closing', icon: <School />, is_timed: true},
    symptomatic_isolation: {name: 'Symptomatic Isolation', icon: <AirlineSeatFlat />, is_timed: false}
};

function AddInterventionDialog({onClose, initialType=Object.keys(INTERVENTIONS)[0], update, initialMinimumAge=70,
                                ...rest}) {
    const [type, setType] = React.useState(initialType);
    const [minimumAge, setMinimumAge] = React.useState(initialMinimumAge);
    return <Dialog {...rest}>
        <DialogTitle>Add/Edit Intervention</DialogTitle>
        <DialogContent style={{width: '30em'}}>
            <Grid container direction='column' spacing={4} alignItems='stretch'>
                <Grid item>
                    <Select value={type} onChange={ev => setType(ev.target.value)} label='Intervention type'>
                        {Object.entries(INTERVENTIONS).map(([type, {name, icon}]) =>
                            <MenuItem value={type}>
                                <Grid container direction='row' alignItems='center' spacing={2}>
                                    <Grid item>{icon}</Grid>
                                    <Grid item>{name}</Grid>
                                </Grid>
                            </MenuItem>)}
                    </Select>
                </Grid>
                {(type === 'elderly_distancing') && <>
                    <Grid item container spacing={2} direction='row'>
                        <Grid item>Minimum age</Grid>
                        <Grid item>
                            <Slider value={minimumAge} onChange={(ev, newMinimumAge) => setMinimumAge(newMinimumAge)}
                                    valueLabelDisplay='auto' style={{width: '20em'}}/>
                        </Grid>
                    </Grid>
                </>}
                <Grid item container direction='column' alignItems='center'>
                    <Grid container direction='row' justify='center'>
                        <Button onClick={onClose}>Cancel</Button>
                        <Button variant='contained' color='primary'
                                onClick={() =>
                                    update({type, minimumAge: type === 'elderly_distancing' ? minimumAge : undefined})}>
                            Done
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
        </DialogContent>
    </Dialog>;
}

function InterventionListItem({intervention: {type, compliance, startDate, duration, ...interventionProps},
                               simulationLength, update}) {
    return <ListItem>
        <ListItemIcon>
            <Tooltip title={INTERVENTIONS[type].name}>
                <Grid container direction='column' alignItems='center' style={{marginRight: '2em', width: '3em'}}>
                    {INTERVENTIONS[type].icon}
                    <span style={{fontSize: '.7em'}}>
                        {type === 'elderly_distancing' && ` (age ${interventionProps.minimumAge}+)`}
                    </span>
                </Grid>
            </Tooltip>
        </ListItemIcon>
        <ListItemText>
            <Grid container direction='row' alignItems='center' spacing={3}>
                <Grid item>
                    <Grid container direction='column' alignItems='center'>
                        <span style={{textTransform: 'uppercase', fontSize: '.7em'}}>Compliance</span>
                        <Slider
                            style={{width: '5em'}}
                            value={compliance * 100}
                            onChange={(ev, newValue) => update({type, compliance: newValue / 100, startDate, duration,
                                                                ...interventionProps})}
                            valueLabelDisplay='auto'
                            valueLabelFormat={value => `${value}%`}
                        />
                    </Grid>
                </Grid>
                {INTERVENTIONS[type].is_timed &&
                <Grid item>
                    <Grid container direction='column' alignItems='center'>
                        <span style={{textTransform: 'uppercase', fontSize: '.7em'}}>Duration</span>
                        <Slider
                            style={{width: '12em'}}
                            min={0}
                            max={simulationLength}
                            value={[startDate, startDate + duration]}
                            valueLabelDisplay='auto'
                            aria-labelledby='range-slider'
                            onChange={
                                (ev, [newStartDate, newEndDate]) => update({
                                    type, compliance, startDate: newStartDate, duration: newEndDate - newStartDate,
                                    ...interventionProps})}
                            valueLabelFormat={value => `day ${value}`}
                        />
                    </Grid>
                </Grid>}
            </Grid>
        </ListItemText>
        <ListItemSecondaryAction>
            <IconButton onClick={() => update(null)}>
                <Clear />
            </IconButton>
        </ListItemSecondaryAction>
    </ListItem>
}


export default function StartSimulationDialog({onClose, startedSimulation, ...rest}) {
    const [error, setError] = React.useState(null);
    const [cities, setCities] = React.useState(null);
    React.useEffect(() => {
        if (cities === null) {
            api('cities').then(response => setCities(response)).catch(setError);
        }
    });
    const [city, setCity] = React.useState({name: null, population: null});
    const [initialInfections, setInitialInfections] = React.useState('');
    const initialInfectionsValid = !isNaN(initialInfections) && Number.isInteger(parseFloat(initialInfections)) &&
                                   initialInfections > 0 && initialInfections < city.population;
    const [simulationLength, setSimulationLength] = React.useState(null);
    const simulationLengthValid = !isNaN(simulationLength) && Number.isInteger(parseFloat(simulationLength));
    const canStart = !!city.name && initialInfectionsValid && simulationLengthValid;
    const [interventions, setInterventions] = React.useState([]);
    const [addInterventionDialogOpen, setAddInterventionDialogOpen] = React.useState(false);

    const start = () => {
        api('simulation', {city: city.name, initial_infections: initialInfections, initial_date: 0,
                           interventions: interventions, num_days: simulationLength},  'POST')
            .then(({simulation_id}) => startedSimulation(simulation_id))
            .catch(error => setError(error.toString()));
    };

    return <Dialog {...rest}>
        <DialogTitle>Start new simulation</DialogTitle>
        <DialogContent style={{width: '30em'}}>
            <Grid container direction='column' spacing={2}>
                <Grid item>
                    <TextField label='Simulation length (days)' type='number' value={simulationLength}
                               onChange={ev => setSimulationLength(ev.target.value)}
                               error={simulationLength && !simulationLengthValid}
                               helperText={simulationLength && !simulationLengthValid && 'Whole number required'}
                    />
                </Grid>
                <Grid item>
                    {cities && <Autocomplete
                        options={cities.map(c => c.name)}
                        getOptionLabel={option => option}
                        inputValue={city.name}
                        onChange={(event, newValue) => setCity(cities.find(c => c.name === newValue))}
                        renderInput={params => <TextField {...params} label="Select city" margin="normal" />}
                    />}
                </Grid>
                {city.name && <Grid item container>
                    <Grid container direction='row' alignItems='end' spacing={2}>
                        <Grid item>
                            <TextField label='Initial infections' type='number' value={initialInfections}
                                       onChange={ev => setInitialInfections(ev.target.value)}
                                       error={initialInfections && !initialInfectionsValid}
                                       helperText={initialInfections && !initialInfectionsValid
                                                   && `Whole number between 0 and ${city.population} required`}
                            />
                        </Grid>
                        <Grid item style={{flexGrow: 1}}>
                            <Slider
                                style={{marginTop: '2em'}}
                                min={0}
                                max={city.population}
                                value={initialInfections}
                                valueLabelDisplay='on'
                                onChange={(ev, newValue) => setInitialInfections(newValue)}
                            />
                        </Grid>
                    </Grid>
                </Grid>}
                {simulationLengthValid && <Grid item>
                    <List>
                        {interventions.map(
                            (intervention, i) =>
                                <InterventionListItem
                                    intervention={intervention}
                                    simulationLength={simulationLength}
                                    update={newIntervention =>
                                        setInterventions(interventions.slice(0, i)
                                            .concat(newIntervention === null ? [] : [newIntervention],
                                                    interventions.slice(i + 1)))}
                                />
                        )}
                        <ListItem button onClick={() => setAddInterventionDialogOpen(true)}>
                            <ListItemIcon>
                                <Add />
                            </ListItemIcon>
                            <ListItemText primary='Add intervention' />
                        </ListItem>
                    </List>
                </Grid>}
                <Grid item>
                    <Grid container direction='column' alignItems='center' spacing={2}>
                        <Grid item style={{color: 'red'}}>
                            {error}
                        </Grid>
                        <Grid item>
                            <Grid container direction='row' justify='center'>
                                <Button onClick={onClose}>Cancel</Button>
                                <Button variant='contained' color='primary' disabled={!canStart} onClick={start}>
                                    Start
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </DialogContent>
        <AddInterventionDialog open={addInterventionDialogOpen} onClose={() => setAddInterventionDialogOpen(false)}
                               update={({type, ...props}) => {
                                   setInterventions(interventions.concat([{
                                       type,
                                       startDate: INTERVENTIONS[type].is_timed ? 0 : undefined,
                                       duration: INTERVENTIONS[type].is_timed ? simulationLength : undefined,
                                       compliance: .5,
                                       ...props
                                   }]));
                                   setAddInterventionDialogOpen(false);
                               }} />
    </Dialog>
};