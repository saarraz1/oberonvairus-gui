import React, {useEffect, useState} from 'react';
import './App.css';
import StartSimulationDialog from "./StartSimulationDialog";
import Paper from "../node_modules/@material-ui/core/Paper/Paper";
import Button from "../node_modules/@material-ui/core/Button/Button";
import {api} from "./api";
import Typography from "../node_modules/@material-ui/core/Typography/Typography";

function App() {
    const [error, setError] = useState(null);
    const [simulations, setSimulations] = useState(null);
    useEffect(() => {
        if (simulations === null) {
            api('simulation').then(setSimulations).catch(err => setError(err.toString()));
        }
    });
    const [startSimulationDialogOpen, setStartSimulationDialogOpen] = useState(false);
    return (<div className='App'>
        <Paper>
            <Button variant='contained' onClick={() => setStartSimulationDialogOpen(true)} color='primary'>
                Start new simulation
            </Button>
            {error}
            <Typography variant='h4'>Simulations</Typography>
            {simulations}
        </Paper>
        <StartSimulationDialog
            open={startSimulationDialogOpen}
            onClose={() => setStartSimulationDialogOpen(false)}
            startedSimulation={simulation_id => setSimulations(simulations.concat([simulation_id]))}
        />
    </div>);
}

export default App;
