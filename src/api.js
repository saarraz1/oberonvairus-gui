export function api(path, json, method='GET', options={}) {
    return fetch(`/api/${path}`, {credentials: 'include',
                                  method: method,
                                  body: json ? JSON.stringify(json) : undefined,
                                  headers: json ? {'Content-Type': 'application/json'} : undefined,
                                  ...options})
        .then(async response => {
            if (response.status >= 200 && response.status < 300) {
                return response;
            }
            const error = new Error(response.statusText);
            error.status = response.status;
            error.message = await response.json();
            throw error;
        })
        .then(response => response.json())
}